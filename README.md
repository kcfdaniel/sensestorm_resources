# SenseStorm Resources

(Legacy) **asoundrc**: ~/.asoundrc

**device_config.json**: ~/.config/googlesamples-assistant/device_config.json

**credentials.json**: ~/.config/google-oauthlib-tool/credentials.json

(Legacy) **client_secret_921971583116-o497r6li2erll7l79g8pksqhpgc8s2f2.apps.googleusercontent.com.json**: no use for now

**sensestorm_object_detection.py**: workshop session 6 object detection

**labelmap.txt**:  workshop session 6 object detection

**mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite**:  workshop session 6 object detection

**edgetpu_ssd_v2_traffic_signs.tflite**: for demo only

**traffic_signs_labelmap.txt**: for demo only

**sensestorm_sync.sh**: ~/.sensestorm_sync/sensestorm_sync.sh (sync sensestorm_workshop folder)
