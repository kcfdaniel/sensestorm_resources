#!/bin/bash

if test -d ~/Desktop/sensestorm_workshop/; then
    # sync sensestorm_workshop
    cd ~/Desktop/sensestorm_workshop/

    git remote update

    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    if [ $LOCAL = $REMOTE ]; then
        echo "Up-to-date"
    elif [ $LOCAL = $BASE ]; then
        echo "Need to pull"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard origin/master
        git pull git@gitlab.com:kcfdaniel/sensestorm_workshop.git
        git clean -fd
    elif [ $REMOTE = $BASE ]; then
        echo "Need to push"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard origin/master
        git pull git@gitlab.com:kcfdaniel/sensestorm_workshop.git
        git clean -fd
    else
        echo "Diverged"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard origin/master
        git pull git@gitlab.com:kcfdaniel/sensestorm_workshop.git
        git clean -fd
    fi
else
    cd ~/Desktop/
    git clone git@gitlab.com:kcfdaniel/sensestorm_workshop.git
fi

# sync sensestorm python package
# update
if [ -d ~/.local/lib/python3.5/site-packages/sensestorm/ ] || [ -d ~/.local/lib/python3.7/site-packages/sensestorm/ ] ; then
    if [ -d ~/.local/lib/python3.5/site-packages/sensestorm/ ] ; then
        cd /home/pi/.local/lib/python3.5/site-packages/sensestorm/
    else
        cd /home/pi/.local/lib/python3.7/site-packages/sensestorm/
    fi

    git remote update

    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    if [ $LOCAL = $REMOTE ]; then
        echo "Up-to-date"
    elif [ $LOCAL = $BASE ]; then
        echo "Need to pull"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard
        git pull
        git clean -fd
    elif [ $REMOTE = $BASE ]; then
        echo "Need to push"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard
        git pull
        git clean -fd
    else
        echo "Diverged"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard
        git pull
        git clean -fd
    fi
else
    # create the sensestorm package if it's not there yet
    if [ -d ~/.local/lib/python3.5/site-packages/ ] ; then
        cd ~/.local/lib/python3.5/site-packages/
        git clone git@gitlab.com:kcfdaniel/sensestorm.git
    elif [ -d ~/.local/lib/python3.7/site-packages/ ] ; then
        cd ~/.local/lib/python3.7/site-packages/
        git clone git@gitlab.com:kcfdaniel/sensestorm.git
        cd sensestorm
        git checkout master2
    else
        echo "Error: Neither Python 3.5 or 3.7 seems to be installed"
    fi
fi

if test -d ~/Desktop/car-simulator-python/; then
    # sync car-simulator-python
    cd ~/Desktop/car-simulator-python/

    git remote update

    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    if [ $LOCAL = $REMOTE ]; then
        echo "Up-to-date"
    elif [ $LOCAL = $BASE ]; then
        echo "Need to pull"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard origin/vanple
        git pull git@VanpleW:VanpleW/car-simulator-python.git
        git clean -fd
    elif [ $REMOTE = $BASE ]; then
        echo "Need to push"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard origin/vanple
        git pull git@VanpleW:VanpleW/car-simulator-python.git
        git clean -fd
    else
        echo "Diverged"
        find */ -name ".git" -type d -exec rm -rf {} +
        git stash --all
        git reset --hard origin/vanple
        git pull git@VanpleW:VanpleW/car-simulator-python.git
        git clean -fd
    fi
else
    cd ~/Desktop/
    git clone git@VanpleW:VanpleW/car-simulator-python.git
fi
sleep 1